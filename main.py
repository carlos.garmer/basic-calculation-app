#Autor Carlos García Mercado A01633757

import random

#Seciones para cada funcion, s <- significa que es local

def suma ():
    s_correctas = 0
    s_res = ""
    #Nivel 1
    for i in range(5):
        a = random.randint(1,9)
        b = random. randint(1,9)
        print(str(a) + " + " + str(b) + " =")
        s_res = int(input("Escribe la respuesta correcta a la suma >>>"))
        if s_res == a+b:
            s_correctas +=1
    #Nivel 2
    if s_correctas == 5:
        for i in range(5):
            a = random.randint(1,9)
            b = random. randint(10,99)
            print(str(a) + " + " + str(b) + " =")
            s_res = int(input("Escribe la respuesta correcta a la suma >>>"))
            if s_res == a+b:
                s_correctas +=1
    if s_correctas == 10:
        print(f"{nombre} , entraste al salon de la fama")
    s_correctas = 0

def resta():
    s_res = 0
    for i in range(5):
        a = random.randint(10,99)
        b = random. randint(10,99)
        while s_res != a-b:
            print(str(a) + " - " + str(b) + " =")
            s_res = int(input("Escribe la respuesta correcta a la resta >>>"))
        
def mult() :
    s_aciertos = 0
    for i in range(10):
        a = random.randint(1,9)
        b = random. randint(1,9)
        print(str(a) + " * " + str(b) + " =")
        s_res = int(input("Escribe la respuesta correcta a la multiplicacion >>>"))
        if s_res == a*b:
            s_aciertos +=1
            print("CORRECTA")
        else :
            print("INCORRECTA")
    print(f"Tu número de aciertos fue {s_aciertos}")
        
        
#Menu
print("Bienvenido al programa de practica de calculo mental")
nombre= input("Ingresa tu nombre >>>")
res = ""

while res != "SALIR" :
    print("""
    
    MENU
    
    SUMA
    RESTA
    MULTIPLICACION
    
    SALIR
    """)
    res= input("Operación a practicar >>>")
    
    if res == "SUMA" or res == "suma":
        suma()
    elif res == "RESTA" or res == "resta":
        resta()
    elif res == "MULTIPLICACION" or res == "multiplicacion":
        mult()
    elif res == "SALIR":
        print("Gracias por usar el programa")
    else :
        print("Opcion invalida")
    